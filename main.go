package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/oid4vip/model/presentation"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging/common"
	pluginSdk "gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugins/core"
	"log"
	"net/url"
)

const (
	PresentationAuthorizationType       = "verifier.presentation.authorization"
	PresentationAuthorizationErrorType  = "verifier.presentation.authorization.error"
	PresentationAuthorizationRemoteType = "verifier.presentation.authorization.remote"

	PresentationRequestNotification = "presentationRequest"

	PresentationRequestTTL = 604800 // 1 week in seconds

	TenantId    = "tenant_space"
	ServiceName = "nats-wrapper-service"

	AuthorizationTopic = "presentation.authorisation"
)

// const userId = "2eec8abb-c0e2-4e9a-87a6-88c8fdadeed4"
const userId = "01d911fa-3318-477c-9913-c3d7ed4100b0"

//const userId = "4c216ab0-a91a-413f-8e97-a32eee7a4ef4"

type PresentationAuthorizationCreationRequest struct {
	common.Request
	PresentationDefinition presentation.PresentationDefinition `json:"presentationDefinition"`
	Ttl                    int                                 `json:"ttl"`
	TenantUri              string                              `json:"tenant_uri"`
	TargetUri              string                              `json:"target_uri"`
	RequestObjectUri       string                              `json:"requestobject_uri"`
	Nonce                  []byte                              `json:"nonce"`
}

type PresentationAuthorizationCreationReply struct {
	BaseReply      common.Reply
	PresentationId string `json:"presentation_id"`
	RequestUri     string `json:"request_uri"`
}

type PresentationAuthorizationRemoteRequest struct {
	common.Request
	ClientId   string `json:"clientId"`
	RequestUri string `json:"request_uri"`
	Ttl        int    `json:"ttl"`
}

type PresentationRequestRecord struct {
	messaging.HistoryRecord
	TTL int `json:"ttl"`
}

func main() {
	//PresentationRequestNatsFlow()

	GeneratePresentationRedirectUrl()
}

func PresentationRequestNatsFlow() {
	config, broker := getBroker()

	reply, err := requestPresentation(config, broker)
	if err != nil {
		return
	}
	rb, _ := json.Marshal(reply)
	fmt.Println(string(rb))
	err = handleRedirect(reply.RequestUri, broker)
	if err != nil {
		fmt.Printf("could not handle redirect: %t", err)
		return
	}
	notifyHolder(reply.PresentationId, broker)
}

func GeneratePresentationRedirectUrl() {
	pd := getDefinitionWithConstraints("Al")
	//pd := getDefinitionWithConstraints("Harry")
	pdBytes, err := json.Marshal(pd)
	if err != nil {
		log.Fatalf("JSON marshaling failed: %s", err)
	}
	pdEncoded := base64.URLEncoding.EncodeToString(pdBytes)
	id, requestId := uuid.NewString(), uuid.NewString()
	tenantId := "tenant_space"
	ttl := 604800
	requestUrl := fmt.Sprintf("https://cloud-wallet.xfsc.dev/api/presentation/request?tenantId=%s&id=%s&requestId=%s&groupId=&ttl=%d&presentationDefinition=%s", tenantId, id, requestId, ttl, pdEncoded)
	requestUrlEscaped := url.QueryEscape(requestUrl)
	//authUrl := "https%3A%2F%2Fauth-cloud-wallet.xfsc.dev%2Frealms%2Freact-keycloak%2Fprotocol%2Fopenid-connect%2Fauth%3Fclient_id%3Dreact-keycloak%26redirect_uri%3Dhttps%253A%252F%252Fcloud-wallet.xfsc.dev%252Fen%252Fwallet%252Fcredentials%26response_type%3Dcode%26scope%3Dopenid%2Bprofile%26state%3D06f6feafb58d4b99b4fcf9df93376cf2%26code_challenge%3DSrQuvTgvsgcHhr5A7KdDuRnsH9JZ9Rjk9kfdUWco_Hc%26code_challenge_method%3DS256%26response_mode%3Dquery"
	authUrl := "https://cloud-wallet.xfsc.dev/en/wallet/selection"
	reqUrl := fmt.Sprintf("https://cloud-wallet.xfsc.dev/api/presentation/authorize?client_id=mytest&request_uri=%s&authUrl=%s", requestUrlEscaped, authUrl)
	fmt.Println(reqUrl)
}

func handleRedirect(requestUri string, broker pluginSdk.EventBus) error {
	data := PresentationAuthorizationRemoteRequest{
		Request: common.Request{
			TenantId:  TenantId,
			RequestId: uuid.NewString(),
		},

		ClientId:   "",
		RequestUri: requestUri,
		Ttl:        PresentationRequestTTL,
	}
	bdata, _ := json.Marshal(data)

	ev, _ := cloudeventprovider.NewEvent(ServiceName, PresentationAuthorizationRemoteType, bdata)
	return broker.Publish(context.Background(), AuthorizationTopic, ev)
}

func requestPresentation(config pluginSdk.Config, broker pluginSdk.EventBus) (*PresentationAuthorizationCreationReply, error) {
	req := PresentationAuthorizationCreationRequest{
		Request: common.Request{
			TenantId:  TenantId,
			RequestId: uuid.NewString(),
		},
		PresentationDefinition: getDefinitionWithConstraints("Dr"),
		Ttl:                    PresentationRequestTTL, // one week
		TenantUri:              "credential-verification-service.default.svc.cluster.local:8080/v1/tenants/tenant_space",
		RequestObjectUri:       "credential-verification-service.default.svc.cluster.local:8080/v1/tenants/tenant_space/presentation/proof",
		TargetUri:              "credential-verification-service.default.svc.cluster.local:8080/v1/tenants/tenant_space/presentation",
		Nonce:                  []byte{34, 34, 11},
	}
	data, _ := json.Marshal(req)
	ev, _ := cloudeventprovider.NewEvent(config.Name, PresentationAuthorizationType, data)
	rep, err := broker.Request(context.Background(), "request", ev)
	if err != nil {
		fmt.Printf("Received error from nats: %s", err.Error())
		return nil, err
	}
	var reply PresentationAuthorizationCreationReply
	_ = rep.DataAs(&reply)
	fmt.Println(string(rep.Data()))
	fmt.Println(string(rep.Type()))
	return &reply, err
}

func getBroker() (pluginSdk.Config, pluginSdk.EventBus) {
	config := pluginSdk.Config{
		LogLevel: "info",
		IsDev:    true,
		Name:     ServiceName,
		Tenant:   TenantId,
		Nats: struct {
			Url        string `envconfig:"PLUGIN_NATS_URL"`
			QueueGroup string `envconfig:"PLUGIN_NATS_QUEUEGROUP"`
		}(struct {
			Url        string
			QueueGroup string
		}{Url: "localhost:4222", QueueGroup: "nats-wrapper-service"}),
		Crypto: struct {
			Namespace string `envconfig:"PLUGIN_CRYPTO_NAMESPACE"`
		}(struct{ Namespace string }{Namespace: ""}),
	}
	pluginSdk.SetLibConfig(config)
	broker := pluginSdk.NewEventBus()
	return config, broker
}

func notifyHolder(presentationId string, broker pluginSdk.EventBus) {
	notification :=
		PresentationRequestRecord{
			HistoryRecord: messaging.HistoryRecord{
				Reply: common.Reply{
					TenantId:  TenantId,
					RequestId: presentationId,
					Error:     nil,
				},
				UserId:  userId,
				Message: "presentation request received",
			},
			TTL: PresentationRequestTTL,
		}
	nb, _ := json.Marshal(notification)
	accEv, _ := cloudeventprovider.NewEvent(ServiceName, PresentationRequestNotification, nb)
	err := broker.Publish(context.Background(), "accounts.record", accEv)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("published event of type ", accEv.Type())
	}
}

func getDefinitionWithConstraints(searchValue string) presentation.PresentationDefinition {
	field := presentation.Field{
		Path: []string{"$.credentialSubject"},
		Filter: &presentation.Filter{
			Pattern: searchValue,
		},
	}
	constraints := presentation.Constraints{
		LimitDisclosure: "",
		Fields:          []presentation.Field{field},
	}
	inputDescriptor := presentation.InputDescriptor{
		Description: presentation.Description{
			Id:         uuid.NewString(),
			FormatType: "ldp_vc",
		},
		Format:      presentation.Format{LDPVP: &presentation.FormatSpecification{}},
		Constraints: constraints,
		Group:       []string{},
	}
	result := presentation.PresentationDefinition{
		Description: presentation.Description{
			Id:         uuid.NewString(),
			Name:       "test",
			Purpose:    "I wanna see it!",
			FormatType: "ldp_vp",
		},
		InputDescriptors: []presentation.InputDescriptor{inputDescriptor},
		Format:           presentation.Format{LDPVP: &presentation.FormatSpecification{}},
	}
	return result
}

func buildConstraints(searchValue string) presentation.PresentationDefinition {
	field := presentation.Field{
		Path: []string{"$.credentialSubject"},
		Filter: &presentation.Filter{
			Pattern: searchValue,
		},
	}
	constraints := presentation.Constraints{
		LimitDisclosure: "",
		Fields:          []presentation.Field{field},
	}
	inputDescriptor := presentation.InputDescriptor{
		Format:      presentation.Format{},
		Constraints: constraints,
	}
	result := presentation.PresentationDefinition{
		Description: presentation.Description{
			Id: uuid.NewString(),
		},
		InputDescriptors: []presentation.InputDescriptor{inputDescriptor},
		Format:           presentation.Format{LDPVP: &presentation.FormatSpecification{}},
	}
	return result
}
